variable "project_id" {
  description = "The project in which the buckets will be created"
}

variable "gcs_location" {
  description = "The location of the bucket"
  default     = "australia-southeast1"

}

variable "labels" {
  description = "labels to be applied"
  default     = {}
}

variable "create_common_buckets" {
  type        = bool
  default     = true
  description = "If set to false the common buckets (data, code) are not created"
}

variable "extra_buckets" {
  type        = map(any)
  default     = {}
  description = "A mapping of extra buckets to be created"
}
