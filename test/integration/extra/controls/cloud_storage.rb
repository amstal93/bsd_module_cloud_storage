title 'Verifying the extra cloud storage has been set up correctly'

PROJECT_ID = attribute('project_id', description: 'The project the buckets are created in')
GCS_LOCATION = attribute('gcs_location', description: 'The default location of gcs storage')
EXTRA_BUCKET_NAMES = attribute('extra_bucket_names', description: 'the list of extra bucket names that have been created')
control "extra-buckets" do
  title 'Ensure the extra buckets have been created and versioning is disabled'
  EXTRA_BUCKET_NAMES.each do |bucket|
    describe google_storage_bucket(name: bucket) do
      it { should exist }
      its('storage_class') { should eq "STANDARD" }
      its('location') { should eq GCS_LOCATION }
      its('versioning.enabled') { should eq nil}
    end
  end
end
