


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| gcs\_location | The location of the bucket | `string` | `"australia-southeast1"` | no |
| labels | labels to be applied | `map` | <pre>{<br>  "org": "bsd"<br>}</pre> | no |
| project\_id | The project in which the buckets will be created | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| common\_bucket\_names | The names of the common bucket made |
| extra\_bucket\_names | The names of the extra bucket made |
| project\_id | The ID of the project in which resources are provisioned. |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->