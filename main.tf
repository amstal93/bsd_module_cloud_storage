locals {

  common_buckets = {
    "data" = {
      bucket_name   = "${var.project_id}-data-storage",
      labels        = { use : "data" },
      force_destroy = false,
      storage_class = "STANDARD"
    },
    "code" = {
      bucket_name   = "${var.project_id}-code-storage",
      labels        = { use : "code" },
      force_destroy = false,
      storage_class = "STANDARD"
    }
  }

  bucket_labels = {
    tf : "cloud_storage"
  }
}

resource "google_storage_bucket" "common_buckets" {
  for_each      = var.create_common_buckets ? local.common_buckets : {}
  name          = each.value.bucket_name
  project       = var.project_id
  location      = var.gcs_location
  force_destroy = each.value.force_destroy
  storage_class = each.value.storage_class
  labels        = merge(var.labels, local.bucket_labels, each.value.labels)

}

resource "google_storage_bucket" "extra_buckets" {
  for_each      = var.extra_buckets
  name          = "${var.project_id}-${each.value.bucket_name}"
  project       = var.project_id
  location      = var.gcs_location
  force_destroy = each.value.force_destroy
  labels        = merge(var.labels, local.bucket_labels, each.value.labels)
  storage_class = each.value.storage_class
}
