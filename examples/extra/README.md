# Extra Example

This example illustrates how to use the `bsd_module_cloud_storage` module.

```hcl
locals {
  extra_buckets = {
    "horse" = {
      bucket_name   = "horse",
      labels        = { use : "horse" },
      force_destroy = false,
      storage_class = "STANDARD"
    },
    "dog" = {
      bucket_name   = "dog",
      labels        = { use : "code" },
      force_destroy = false,
      storage_class = "STANDARD"
    }
  }
}

module "bsd_cloud_storage" {
  source = "git::git@gitlab.com:bluestreetdata/terraform_modules/bsd_module_cloud_storage.git//?ref=tags/v0.0.1"
  project_id  = "<PROJECT ID>"
  extra_buckets = locals.extra_buckets
  create_common_buckets = false
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| create\_common\_buckets | If set to false the common bucket will not be created | `bool` | `true` | no |
| extra\_buckets | A mapping of extra buckets to be created - bucket names will be `{project_id}-{name}` | `map(any)` | `{}` | no |
| gcs\_location | The location of the bucket | `string` | `"australia-southeast1"` | no |
| labels | labels to be applied | `any` | n/a | yes |
| project\_id | The project in which the buckets will be created | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| common\_bucket\_names | The names of the common bucket made |
| extra\_bucket\_names | The names of the extra buckets made |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

To provision this example, run the following from within this directory:
- `terraform init` to get the plugins
- `terraform plan` to see the infrastructure plan
- `terraform apply` to apply the infrastructure build
- `terraform destroy` to destroy the built infrastructure
